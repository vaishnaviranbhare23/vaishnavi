**THEORY:**

&nbsp;

Superposition theorem states that-

"In a linear, bilateral network,
consisting of several sources, the resultant current in any branch is the
algebraic sum of the currents caused by the separate independent sources acting
alone replacing all other sources by their respective internal resistances."

&nbsp;

This theorem when used for
evaluating response in a complicated network containing several sources,
simplifies the analysis. The theorem is particularly used in case of network,
where sources generating voltages or currents of different frequencies are
acting simultaneously, considering the effect of individual source independent
of others.

&nbsp;

&nbsp;

**CIRCUIT DIAGRAM:**

&nbsp;

![Image](image001.jpg)</p>

&nbsp;

&nbsp;

a ) For switch 'S<sub>1</sub>' in
position '1' and 'S<sub>2</sub>' in position '3' , source 'r-n' is active ( ON
) and  source 'y-n' is inactive (
OFF ). Wattmeter pressure coil is to be connected across 'r-n'.<

b ) For switch 'S<sub>1</sub>' in
position '2' and 'S<sub>2</sub>' in position '4' , source 'y-n' is active ( ON
) and source 'r-n' is inactive (
OFF ). Wattmeter pressure coil is to be connected across 'y-n'.

c) For switch 'S<sub>1</sub>' in
position '1' and 'S<sub>2</sub>' in position '4', both sources are active (ON).


Wattmeter pressure coil is to
be connected across 'r-n'.

&nbsp;

&nbsp;

&nbsp;

